#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cii/except.h>
#include <cii/mem.h>
#include <cii/assert.h>
#include "html_escape.h"
#include "debug.h"

#define ENTSZ 8
#define EQUAL(s, d) (0==strcasecmp((s),(d)))


Except_T HTML_ESCAPE_Failed = { "Escape/Unescape character failed" };
static char unescape(const char *);

char* html_escape(char* src) {
	assert(src);

	char *ps = src, *escaped = NULL, *pe = NULL;

	/* scan src, count space needed to hold all characters including escaped characters */
	int c = 0;
	for (ps = src; '\0' != *ps; ps++, c++) {
		switch (*ps) {
			case '>':
			case '<':
				c +=4;
				continue;
			case '&':
				c +=5;
				continue;
			case '"':
				c +=6;
				continue;
		}

	}

	escaped = pe = ALLOC(c + 1);
	for (ps = src; '\0' != *ps; ps++, pe++) {
		switch (*ps) {
			case '>':
				*pe++ = '&';
				*pe++ = 'g';
				*pe++ = 't';
				*pe   = ';';
				continue;
			case '<':
				*pe++ = '&';
				*pe++ = 'l';
				*pe++ = 't';
				*pe   = ';';
				continue;
			case '&':
				*pe++ = '&';
				*pe++ = 'a';
				*pe++ = 'm';
				*pe++ = 'p';
				*pe   = ';';
				continue;
			case '"':
				*pe++ = '&';
				*pe++ = 'q';
				*pe++ = 'u';
				*pe++ = 'o';
				*pe++ = 't';
				*pe   = ';';
				continue;
			default:
				*pe = *ps;
		}
	}

	*pe = '\0';
	return escaped;
}


/* return unescaped html string
 * free returned pointer after using
 */
char* html_unescape(char* src) {
	assert(src);

	char* unescaped = NULL;
	int len = strlen(src);

	unescaped = ALLOC(len+1);
	char *ps = src, *pu = unescaped;
	char entity[ENTSZ];
	while (*ps != '\0') {
		if ('&' != *ps) {
			*pu++ = *ps++;
			continue;
		} else {
			int i;
			char *ps1 = ps;
			for(i = 0;
			    i < sizeof(entity)-2 && ';' != *ps1 && '\0' != *ps1;
			    i++, ps1++) {
				entity[i] = *ps1;
			}

			if (';' != *ps1) { /* wrong html escape sequence */
				entity[i] = '\0';
				fprintf(stderr, "wrong html escape sequence: %s", entity);
				RAISE(HTML_ESCAPE_Failed);
				*pu++ = *ps++;
				continue;
			}

			entity[i++] = *ps1;
			entity[i] = '\0';

			*pu = unescape(entity);
			if (-1 == *pu) {
				RAISE(HTML_ESCAPE_Failed);
			}
				
			pu++;
			ps += i;
		}
	}
	*pu = '\0';
	return unescaped;
}

/* convert html escaped character to original */
static char unescape(const char *entity) {
	char ent[ENTSZ];
	strncpy(ent, entity, ENTSZ-1);
	ent[ENTSZ-1] = '\0';
	assert('&' == *ent);

	int len = strlen(ent);
	assert(len>2);

	assert(';' == ent[len-1]);
	ent[len-1] = '\0';


	if ('#' == ent[1]) {
		int code = atoi(&ent[2]);
		return (char)code;
	}

	if (EQUAL("nbsp", &ent[1])) return ' ';
	if (EQUAL("quot", &ent[1])) return '"';
	if (EQUAL("amp", &ent[1])) return '&';
	if (EQUAL("lt", &ent[1])) return '<';
	if (EQUAL("gt", &ent[1])) return '>';
	fprintf(stderr, "unknow html entity: %s\n", entity);
	return -1;
}


/* Unit tests */
#include "cutest/CuTest.h"

void TestHtmlEscape_unescape(CuTest *tc) {
	char* e[] = { "&nbsp;", "&amp;", "&quot;", "&lt;", "&gt;", "&#30;", "&#31;", "&#32;", "&#127;", "&#128;" };
	char r[] = { ' ', '&', '"', '<', '>', 30, 31, 32, 127, 128 }; 
	int i = 0;
	for (i=0; i< sizeof(r); i++) {
		CuAssertIntEquals(tc, r[i], unescape(e[i]));
	}
}

void TestHtmlEscape_html_unescape(CuTest *tc) {
	char* html = "<html>hello&amp;world<b>amp:&amp;&nbsp;gt:&gt;&nbsp;lt:&lt;&nbsp;</b></html>";
	char* origin = "<html>hello&world<b>amp:& gt:> lt:< </b></html>";
	char* got = html_unescape(html);
	CuAssertStrEquals(tc, origin, got);
	FREE(got);
}

void TestHtmlEscape_html_escape(CuTest *tc) {
	char* html = "&gt;hello&amp;world&lt;&amp;quot&quot;";
	char* origin = ">hello&world<&quot\"";
	char* got = html_escape(origin);
	CuAssertStrEquals(tc, html, got);
	FREE(got);
}
