#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>

const int buffer_size = 4096;
char* buffer_begin = NULL;
char* write_point = NULL;

int main() {

	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();

	char *p = NULL;
	char *unescaped = NULL;
	int unescaped_len = 0;

	int total_length = 0;

	buffer_begin = malloc(1);

	while(1) {
		buffer_begin = realloc(buffer_begin, total_length + buffer_size);
		write_point = buffer_begin + total_length;

		int sz = fread(write_point, 1, buffer_size, stdin);
		total_length += sz;
		if ( sz < buffer_size ) {
			if (feof(stdin)) {
				unescaped = curl_easy_unescape(curl , buffer_begin, total_length, &unescaped_len);
				fprintf(stderr, "%s", unescaped);
				curl_free(unescaped);
				break;
			} else {
				fprintf(stderr, "error when reading stdin\n");
				exit (EXIT_FAILURE);
			}
		} 
	}
	free(buffer_begin);
	exit (0);
}




